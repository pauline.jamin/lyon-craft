﻿using LiveCoding.Persistence;

namespace LiveCoding.Services
{
    public class BookingService
    {
        private readonly IBarRepository _barRepo;
        private readonly IDevRepository _devRepo;
        private readonly IBookingRepository _bookingRepository;
        private readonly HttpClient _httpClient;

        public BookingService(IBarRepository barRepo,
            IDevRepository devRepo,
            IBookingRepository bookingRepository,
            HttpClient httpClient)
        {
            _barRepo = barRepo;
            _devRepo = devRepo;
            _bookingRepository = bookingRepository;
            _httpClient = httpClient;
        }

        public async Task<bool> ReserveBar()
        {
            var bars = await _barRepo.Get();
            var devs = _devRepo.Get().ToList();

            var numberOfAvailableDevsByDate = new Dictionary<DateTime, int>();
            foreach (var devData in devs)
            {
                foreach (var date in devData.OnSite)
                {
                    if (numberOfAvailableDevsByDate.ContainsKey(date))
                    {
                        numberOfAvailableDevsByDate[date]++;
                    }
                    else
                    {
                        numberOfAvailableDevsByDate.Add(date, 1);
                    }
                }
            }

            var maxNumberOfDevs = numberOfAvailableDevsByDate.Values.Max();

            if (maxNumberOfDevs < devs.Count * 0.5)
            {
                return false;
            }

            var bestDate = numberOfAvailableDevsByDate.First(kv => kv.Value == maxNumberOfDevs).Key;

            foreach (var barData in bars)
            {
                if (barData.Capacity >= maxNumberOfDevs && barData.Open.Contains(bestDate.DayOfWeek))
                {
                    await BookBar(barData.Name, bestDate);
                    _bookingRepository.Save(new BookingData(bestDate, barData.Name));
                    return true;
                }
            }

            return false;
        }

        private async Task BookBar(string name, DateTime dateTime)
        {
            var uri = String.Format($"http://localhost:5220/v1/bars/book?name={0}&dateTime={1}", name, dateTime);
            await _httpClient.PostAsync(uri, null);
        }
    }
}