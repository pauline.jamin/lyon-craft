﻿using LiveCoding.Persistence;
using LiveCoding.Services;

namespace LiveCoding.Api
{
    public static class DependencyInjection
    {
        public static IServiceCollection InitializeDependencyInjection(this IServiceCollection services)
        {
            return services
                .AddScoped(hc => new HttpClient { BaseAddress = new Uri("http://localhost:5220/") })
                .AddScoped<IBarRepository, BarRepository>()
                .AddScoped<IDevRepository, DevRepository>()
                .AddSingleton<IBookingRepository, InMemoryBookingRepository>()
                .AddScoped<BookingService>();
        }
    }
}
