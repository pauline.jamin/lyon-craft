﻿using System.Net.Http.Json;

namespace LiveCoding.Persistence;

public class BarRepository : IBarRepository
{
    private readonly HttpClient _httpClient;
    public BarRepository(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
 
    private async Task<List<BarData>?> GetBarsFromApi()
    {
        return await _httpClient.GetFromJsonAsync<List<BarData>>("http://localhost:5220/v1/bars");
    }

    public async Task<IEnumerable<BarData>> Get()
    {
      return  await GetBarsFromApi() ?? throw new Exception("bars not available");
    }
}