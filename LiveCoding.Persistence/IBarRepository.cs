﻿namespace LiveCoding.Persistence;

public interface IBarRepository
{
    Task<IEnumerable<BarData>> Get();
}