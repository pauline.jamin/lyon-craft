﻿namespace LiveCoding.Persistence;

public record DevData(string Name, DateTime[] OnSite);