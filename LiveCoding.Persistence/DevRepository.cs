﻿using System.Net.Security;
using Newtonsoft.Json;

namespace LiveCoding.Persistence;

public class DevRepository : IDevRepository
{
    public IEnumerable<DevData> Get()
    {
        var json = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "devs.json"));
        var devs = JsonConvert.DeserializeObject<IEnumerable<DevData>>(json);

        return devs;
    }
}