namespace LiveCoding.Persistence;

public record BookingData(DateTime Date, string BarName);