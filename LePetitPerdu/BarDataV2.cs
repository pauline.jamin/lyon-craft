namespace LePetitPerdu;

public record BarDataV2(string Name, int NumberMaxOfPeople, DayOfWeek[] Open, int Note, bool HasFood);