using LePetitPerdu;
using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddEndpointsApiExplorer()
    .AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapGet("/", () => "Bienvenue sur le petit perdu!");

app.MapGet("/v1/bars", () =>
{
    var json = File.ReadAllText("./bars.json");
    var bars = JsonConvert.DeserializeObject<IEnumerable<BarData>>(json);

    return bars;
});

app.MapGet("/v2/bars", () =>
{
    var json = File.ReadAllText("./barsV2.json");
    var bars = JsonConvert.DeserializeObject<IEnumerable<BarDataV2>>(json);

    return bars;
});

app.MapPost("/v1/bars/book", (string name, DateTime dateTime) =>
{
    Console.WriteLine("Bar booked: " + name + " at " + dateTime);

    return 201;
});

app.Run();