﻿namespace LePetitPerdu;

public record BarData(string Name, int Capacity, DayOfWeek[] Open);